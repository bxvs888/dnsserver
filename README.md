# DNSServer

开源DNS服务,实现了RFC1035部分协议内容

# 截图

![DNS日志](docs/images/DNS日志.jpg)

![DNS日志](docs/images/DNS记录.jpg)

![DNS日志](docs/images/DNS递归服务器.jpg)

# 部署方式

克隆仓库,修改mysql数据库信息,打包部署即可

```
git clone https://gitee.com/648823596/dnsserver.git
修改ProdConfig类下的数据库信息
cd dnsserver
mvn clean package
cd target
java -jar -Dspring.profiles.active=prod DNSServer.jar
```

# 反馈

* 提交Issue
* 邮箱: 648823596@qq.com
* QQ群: 958754367

# 开源协议
本软件使用 [GPL](http://www.gnu.org/licenses/gpl-3.0.html) 开源协议!

# 版权声明
未经本人授权,禁止将本软件用作商业用途. 
CopyRight 2021 sunyue@schoolwow.cn