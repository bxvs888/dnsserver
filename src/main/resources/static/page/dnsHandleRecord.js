let dnsHandleRecord = {
    "type": "page",
    "body": [
        {
            "type": "crud",
            "name": "dnsHandleRecordCrud",
            "api": "get:/dnsHandleRecord/getList",
            "quickSaveItemApi": "put:/dnsHandleRecord/update",
            "saveOrderApi": "get:/dnsHandleRecord/changeOrder?ids=${ids}",
            "syncLocation": false,
            "affixHeader": false,
            "footable": true,
            "headerToolbar": [
                "filter-toggler",
                "bulkActions",
                "statistics"
            ],
            "footerToolbar": [
                "switch-per-page",
                "pagination"
            ],
            "bulkActions": [
                {
                    "label": "批量删除",
                    "level": "danger",
                    "actionType": "ajax",
                    "api": "delete:/dnsHandleRecord/delete?ids=${ids}",
                    "confirmText": "确定要批量删除吗?",
                },
            ],
            "filter": {
                "title": "条件搜索",
                "mode": "horizontal",
                "horizontal": {
                    "leftFixed": true
                },
                "controls": [
                    {
                        "type": "group",
                        "controls": [
                            {
                                "type": "text",
                                "name": "domain",
                                "label": "域名",
                            },
                            {
                                "type": "select",
                                "name": "type",
                                "label": "类型",
                                "source": "get:/dnsRecord/getQTypeOptionList",
                            },
                            {
                                "type": "text",
                                "name": "value",
                                "label": "记录值",
                            },
                        ]
                    },
                    {
                        "type": "group",
                        "controls": [
                            {
                                "type": "select",
                                "name": "rcode",
                                "label": "返回码",
                                "source": "get:/dnsRecord/getRCODEOptionList"
                            },
                            {
                                "type": "select",
                                "name": "ra",
                                "label": "是否递归查询",
                                "options": [
                                    {
                                        "label": "全部",
                                        "value": 0
                                    },
                                    {
                                        "label": "非递归查询",
                                        "value": 1
                                    },
                                    {
                                        "label": "递归查询",
                                        "value": 2
                                    },
                                ],
                                "value": 0
                            },
                            {
                                "type": "select",
                                "name": "aa",
                                "label": "是否权威查询",
                                "options": [
                                    {
                                        "label": "全部",
                                        "value": 0
                                    },
                                    {
                                        "label": "非权威回答",
                                        "value": 1
                                    },
                                    {
                                        "label": "权威回答",
                                        "value": 2
                                    },
                                ],
                                "value": 0
                            },
                        ]
                    },
                ],
            },
            "draggable": true,
            "columns": [
                {
                    "name": "id",
                    "label": "ID",
                },
                {
                    "name": "port",
                    "label": "客户端端口",
                },
                {
                    "name": "transactionId",
                    "label": "标识id",
                },
                {
                    "name": "domain",
                    "label": "域名",
                },
                {
                    "name": "type",
                    "label": "类型",
                },
                {
                    "name": "rcode",
                    "label": "返回码",
                },
                {
                    "name": "value",
                    "label": "结果",
                },
                {
                    "name": "ttl",
                    "label": "缓存时间(秒)",
                },
                {
                    "type": "status",
                    "name": "ra",
                    "label": "是否递归查询",
                },
                {
                    "type": "status",
                    "name": "aa",
                    "label": "是否权威回答",
                },
                {
                    "name": "reason",
                    "label": "失败原因",
                },
                {
                    "name": "createdTime",
                    "label": "创建时间",
                    "type": "datetime",
                },
                {
                    "type": "operation",
                    'buttons': [
                        {
                            "label": "删除",
                            "type": "button",
                            "level": "danger",
                            "actionType": "ajax",
                            "api": "delete:/dnsHandleRecord/delete?ids=${id}",
                            "confirmText": "确认删除吗?"
                        },
                    ]
                }
            ]
        }]
};