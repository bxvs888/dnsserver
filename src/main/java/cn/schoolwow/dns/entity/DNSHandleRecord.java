package cn.schoolwow.dns.entity;

import cn.schoolwow.quickdao.annotation.Comment;
import cn.schoolwow.quickdao.annotation.Id;
import cn.schoolwow.quickdao.annotation.TableField;

import java.time.LocalDateTime;

@Comment("DNS处理记录")
public class DNSHandleRecord {
    @Id
    private long id;

    @Comment("客户端端口")
    private int port;

    @Comment("标识id")
    private int transactionId;

    @Comment("域名")
    private String domain;

    @Comment("类型")
    private String type;

    @Comment("返回码")
    private String rcode;

    @Comment("结果")
    private String value;

    @Comment("缓存时间(秒)")
    private int ttl;

    @Comment("是否递归查询")
    private boolean ra;

    @Comment("是否权威回答")
    private boolean aa;

    @Comment("失败原因")
    private String reason;

    @Comment("创建时间")
    @TableField(createdAt = true)
    private LocalDateTime createdTime;

    @Comment("更新时间")
    @TableField(createdAt = true)
    private LocalDateTime updateTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRcode() {
        return rcode;
    }

    public void setRcode(String rcode) {
        this.rcode = rcode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getTtl() {
        return ttl;
    }

    public void setTtl(int ttl) {
        this.ttl = ttl;
    }

    public boolean isRa() {
        return ra;
    }

    public void setRa(boolean ra) {
        this.ra = ra;
    }

    public boolean isAa() {
        return aa;
    }

    public void setAa(boolean aa) {
        this.aa = aa;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
}
