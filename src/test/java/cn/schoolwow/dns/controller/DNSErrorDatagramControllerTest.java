package cn.schoolwow.dns.controller;

import cn.schoolwow.dns.config.DNSServerConfig;
import cn.schoolwow.dns.domain.DNSResponse;
import cn.schoolwow.dns.entity.DNSErrorDatagram;
import cn.schoolwow.quickdao.dao.DAO;
import com.mysql.jdbc.Blob;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DNSServerConfig.class)
@ActiveProfiles("dev")
public class DNSErrorDatagramControllerTest {
    @Resource
    private DAO dao;

    @Resource
    private DNSErrorDatagramController dnsErrorDatagramController;

    @Test
    public void analyze() throws SQLException, IOException {
        List<DNSErrorDatagram> dnsErrorDatagramList = dao.query(DNSErrorDatagram.class)
                .addQuery("normal",false)
                .orderBy("createdTime")
                .execute()
                .getList(DNSErrorDatagram.class);
        for(DNSErrorDatagram dnsErrorDatagram:dnsErrorDatagramList){
            Blob blob = (Blob) dnsErrorDatagram.getBlob();
            System.out.println(blob.length());
            byte[] bytes = blob.getBytes(1, (int) blob.length());
            DNSResponse dnsResponse = new DNSResponse(bytes);
            System.out.println(dnsResponse);
            dao.query(DNSErrorDatagram.class)
                    .addQuery("id",dnsErrorDatagram.getId())
                    .addUpdate("normal",true)
                    .execute()
                    .update();
        }
    }
}