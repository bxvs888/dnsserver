package cn.schoolwow.dns.domain.header;

import cn.schoolwow.dns.domain.DNSRequest;
import cn.schoolwow.dns.domain.DNSResponse;
import cn.schoolwow.dns.domain.header.constant.RCODE;
import cn.schoolwow.dns.domain.question.Question;
import cn.schoolwow.dns.domain.question.constants.QTYPE;
import cn.schoolwow.dns.domain.rr.ResourceRecord;
import org.junit.Test;

import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Enumeration;

public class HeaderTest {
    @Test
    public void t2(){
        int a = 65537;
        short b = (short) a;
        System.out.println(b);
    }

    @Test
    public void t22() throws SocketException {
        Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
        while(nets.hasMoreElements()){
            NetworkInterface networkInterface = nets.nextElement();
            if(!networkInterface.getName().equals("eth3")){
                continue;
            }
            System.out.println(networkInterface);
            System.out.println(networkInterface.getIndex());
//            System.out.println(new String(networkInterface.getHardwareAddress()));
            System.out.println(networkInterface.getDisplayName());
            Enumeration<InetAddress> inetAddressEnumeration = networkInterface.getInetAddresses();
            while(inetAddressEnumeration.hasMoreElements()){
                System.out.println(inetAddressEnumeration.nextElement());
            }
            System.out.println(networkInterface.getMTU());
            System.out.println(networkInterface.getInterfaceAddresses().get(2).getAddress());
            System.out.println();
        }
    }

    @Test
    public void server() throws IOException {
        DatagramSocket socket = new DatagramSocket(1111);
        byte[] data = new byte[512];
        DatagramPacket packet = new DatagramPacket(data, data.length);
        socket.receive(packet);
        System.out.println("客户端说:"+new String(data));

        packet.setData(("服务端说客户端说:"+new String(data)).getBytes());
        socket.send(packet);
        socket.close();
    }

    @Test
    public void client() throws IOException {
        DatagramSocket socket = new DatagramSocket();
        byte[] data = "你好啊".getBytes();
        DatagramPacket packet = new DatagramPacket(data, data.length,InetAddress.getByName("localhost"),1111);
        socket.send(packet);
        System.out.println("客户端发送了包");
        socket.receive(packet);

        data = new byte[1024];
        packet.setData(data);
        System.out.println(new String(packet.getData()));
        socket.close();
    }

    @Test
    public void getID() throws IOException {
        File file = new File(System.getProperty("user.dir")+"/dnsReport.bin");
        FileInputStream fis = new FileInputStream(file);
        DataInputStream dis = new DataInputStream(fis);
        byte b = 0;
        while((b=dis.readByte())!=-1){
            System.out.print(b+",");
        }
        System.out.println();

        DNSRequest dnsMessage = new DNSRequest(dis);
        System.out.println(dnsMessage);

        dis.close();
        fis.close();
    }

    @Test
    public void t3() throws IOException {
        byte[] bytes = Files.readAllBytes(Paths.get(System.getProperty("user.dir")+"/dnsReport.bin"));
        DNSRequest dnsRequest1 = new DNSRequest(new DataInputStream(new ByteArrayInputStream(bytes)));
        System.out.println(dnsRequest1);
    }

    @Test
    public void tt() throws IOException {
        /*
         * 接收客户端发送的数据
         */
        // 1.创建服务器端DatagramSocket，指定端口
        DatagramSocket socket = new DatagramSocket(53);
        // 2.创建数据报，用于接收客户端发送的数据
        byte[] data = new byte[512];// 创建字节数组，指定接收的数据包的大小
        DatagramPacket packet = new DatagramPacket(data, data.length);
        // 3.接收客户端发送的数据
        System.out.println("****服务器端已经启动，等待客户端发送数据");
        socket.receive(packet);// 此方法在接收到数据报之前会一直阻塞
        // 4.读取数据
        byte[] bytes = new byte[packet.getLength()];
        System.arraycopy(data,0,bytes,0,packet.getLength());



        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        DataInputStream dis = new DataInputStream(bais);
        DNSRequest dnsRequest = new DNSRequest(dis);

        System.out.println(dnsRequest);

        /*
         * 向客户端响应数据
         */
        // 1.定义客户端的地址、端口号、数据
        InetAddress address = packet.getAddress();
        int port = packet.getPort();
        System.out.println(port);

        Question question = dnsRequest.getQuestions()[0];
        DNSResponse dnsResponse = dnsRequest.getDNSResponse();
        if(question.getQNAME().equals("a.com")){
            ResourceRecord answer = question.answer();
            answer.setTTL(600);
            answer.setRDATA(QTYPE.A,"192.168.1.100");
            dnsResponse.answer(answer);
        }else{
            dnsResponse.rcode(RCODE.QUERY_REFUSED);
        }
        System.out.println(dnsResponse);
        byte[] data2 = dnsResponse.toByteArray();
        System.out.println(data2.length);
        dnsResponse.writeToFile(System.getProperty("user.dir")+"/src/test/resources/response.bin");
        Files.write(Paths.get(System.getProperty("user.dir")+"/response.bin"),data2,StandardOpenOption.CREATE,StandardOpenOption.WRITE);

        DNSRequest dnsRequest1 = new DNSRequest(new DataInputStream(new ByteArrayInputStream(data2)));
        System.out.println(dnsRequest1);








        // 2.创建数据报，包含响应的数据信息
        packet.setData(data2);
        DatagramPacket packet2 = new DatagramPacket(data2, data2.length, address, port);
        // 3.响应客户端
        socket.send(packet2);
        // 4.关闭资源
        socket.close();
        System.out.println("数据已返回");


//        ServerSocket serverSocket = new ServerSocket();
//        serverSocket.bind(new InetSocketAddress("127.0.0.1",53));
//        System.out.println("accept......");
//        Socket socket = serverSocket.accept();
//        System.out.println("收到信息");
//        Path path = Paths.get(System.getProperty("user.dir")+"/dnsReport.bin");
//        Files.copy(socket.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
//        System.out.println(Files.size(path));
//        socket.close();
//        serverSocket.close();
    }
}